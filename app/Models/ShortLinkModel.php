<?php 
namespace App\Models;
use CodeIgniter\Model;

class ShortLinkModel extends Model
{
    protected $table = 'short_urls';

    protected $primaryKey = 'id';
    
    protected $allowedFields = ['long_url', 'short_code','url_prefix','hits','qrpic','created'];
}