<?php 
namespace App\Controllers;
use App\Models\ShortLinkModel;
use CodeIgniter\Controller;
use chillerlan\QRCode\QRCode;


class ShortLink extends Controller
{
    protected static $chars = "abcdfghjkmnpqrstvwxyz|ABCDFGHJKLMNPQRSTVWXYZ|0123456789";
    protected static $table = "short_urls";
    protected static $checkUrlExists = false;
    protected static $codeLength = 7;
    
    // show users list
    public function index(){
   
        $ShortLinkModel = new ShortLinkModel();
        if($_GET['u']){
            $shortlink = $ShortLinkModel->where('short_code', $_GET['u'])->first();
            
            # header to long url
            header("location:".$shortlink['long_url']);
            exit();
        }

 
        
        $data['users'] = $ShortLinkModel->orderBy('id', 'DESC')->findAll();
        return view('form_list', $data);
    }

    // show add user form
    public function create(){
        
        return view('add_user');
    }
 
    // insert data into database
    public function store() {
        $ShortLinkModel = new ShortLinkModel();

        // Long URL
        $longURL = $this->request->getVar('long_url');
        // Prefix of the short URL 
        $shortURL_Prefix = base_url('/'); // without URL rewrite
      
        try{
            // Get short code of the URL
            $shortCode = $this->makeURLShort($longURL , $shortURL_Prefix);
            
          
        
        }catch(Exception $e){
            // Display error
            echo $e->getMessage();
        }

        return $this->response->redirect(site_url('/form_list'));
    }

    // show single user
    public function singleUser($id = null){
        $ShortLinkModel = new ShortLinkModel();
        $data['user_obj'] = $ShortLinkModel->where('id', $id)->first();
        return view('edit_user', $data);
    }

    // update user data
    public function update(){
        $ShortLinkModel = new ShortLinkModel();
        $id = $this->request->getVar('id');
        $data = [
            'long_url' => $this->request->getVar('long_url')
        ];
        $ShortLinkModel->update($id, $data);
        return $this->response->redirect(site_url('/form_list'));
    }
 
    // delete user
    public function delete($id = null){
        $ShortLinkModel = new ShortLinkModel();
        $data['user'] = $ShortLinkModel->where('id', $id)->delete($id);
        return $this->response->redirect(site_url('/form_list'));
    }    



    
    public function makeURLShort($url ,$shortURL_Prefix){
        if(empty($url)){
            throw new Exception("No URL was supplied.");
        }

        if($this->CheckFormatURL($url) == false){
            throw new Exception("URL does not have a valid format.");
        }

        if(self::$checkUrlExists){
            if (!$this->verifyUrlExists($url)){
                throw new Exception("URL does not appear to exist.");
            }
        }

    
   
        $shortCode = $this->createShortCode($url , $shortURL_Prefix);

        return $shortCode;
    }

    protected function CheckFormatURL($url){
        return filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_HOST_REQUIRED);
    }

    protected function verifyUrlExists($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch,  CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);
        $response = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return (!empty($response) && $response != 404);
    }

    protected function urlExistsInDB($url){
        $query = "SELECT short_code FROM ".self::$table." WHERE long_url = :long_url LIMIT 1";
        
        $stmt = $this->pdo->prepare($query);
        $params = array(
            "long_url" => $url
        );
        $stmt->execute($params);

        $result = $stmt->fetch();
        return (empty($result)) ? false : $result["short_code"];
    }

    protected function createShortCode($url ,$shortURL_Prefix){
        $shortCode = $this->generateRandomString($this->$codeLength);
        
        $id = $this->insertUrlInDB($url, $shortCode , $shortURL_Prefix);
        return $shortCode;
    }
    
    protected function generateRandomString($length = 6){
        $sets = explode('|', self::$chars);
        $all = '';
        $randString = '';
        foreach($sets as $set){
            $randString .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for($i = 0; $i < $length - count($sets); $i++){
            $randString .= $all[array_rand($all)];
        }
        $randString = str_shuffle($randString);
        return $randString;
    }

    protected function insertUrlInDB($url, $code , $shortURL_Prefix){
        
        $ShortLinkModel = new ShortLinkModel();
        $params = array(
            "long_url" => $url,
            "short_code" => $code,
            "url_prefix" => $shortURL_Prefix,
          
            "created" => date('Y-m-d H:i:s')
        );
      

        $id = $ShortLinkModel->insert($params);
   
      
        #create qr code				
         $text = $url;
        // $file_name = $id.'.png';
        $fullpath = "uploaded/qrcode/";
        $shortqrurl = $shortURL_Prefix.'/?u='.$code;
        $qr = new QRCode();
        #update qr pic
        $data = [
            'qrpic' => $id.'.png'
        ];
        $ShortLinkModel->update($id, $data);

        $qr->render($shortqrurl, $fullpath. $id.'.png');
       
        
        return $id;
    }
    
    public function shortCodeToUrl($code, $increment = true){
        if(empty($code)) {
            throw new Exception("No short code was supplied.");
        }

        if($this->validateShortCode($code) == false){
            throw new Exception("Short code does not have a valid format.");
        }

        $urlRow = $this->getUrlFromDB($code);
        if(empty($urlRow)){
            throw new Exception("Short code does not appear to exist.");
        }

        if($increment == true){
            $this->incrementCounter($urlRow["id"]);
        }

        return $urlRow["long_url"];
    }

    protected function validateShortCode($code){
        $rawChars = str_replace('|', '', self::$chars);
        return preg_match("|[".$rawChars."]+|", $code);
    }

    protected function getUrlFromDB($code){
        $query = "SELECT id, long_url FROM ".self::$table." WHERE short_code = :short_code LIMIT 1";
        $stmt = $this->pdo->prepare($query);
        $params=array(
            "short_code" => $code
        );
        $stmt->execute($params);

        $result = $stmt->fetch();
        return (empty($result)) ? false : $result;
    }

    protected function incrementCounter($id){
        $query = "UPDATE ".self::$table." SET hits = hits + 1 WHERE id = :id";
        $stmt = $this->pdo->prepare($query);
        $params = array(
            "id" => $id
        );
        $stmt->execute($params);
    }

}